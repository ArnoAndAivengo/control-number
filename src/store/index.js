import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    controls: [
      {
        id: 1,
        value: 0
      },
      {
        id: 2,
        value: 0
      },
      {
        id: 3,
        value: 0
      }
    ],
  },
  actions: {
    setValue({commit}, payload) {
      commit('setValue', payload)
    },
  },
  mutations: {
    setValue(state, value) {
      state.controls = value
    },
  },
  getters: {
    getState(state) {
      return state.controls
    },
  },
})
